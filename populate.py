from app import app,db
from datetime import datetime
from app.models import *

# add team
team=Team(id=1,teamName='Admin')
db.session.add(team)
team=Team(id=2,teamName='User')
db.session.add(team)


# add rooms
room=Room(id=0,roomName='Admin Room')
db.session.add(room)
room=Room(id=1,roomName='127 Marconi')
db.session.add(room)
room=Room(id=2,roomName='339b Marconi')
db.session.add(room)
room=Room(id=3,roomName='117-118 Fermi')
db.session.add(room)
room=Room(id=4,roomName='314 Fermi')
db.session.add(room)
room=Room(id=5,roomName='413 Fermi')
db.session.add(room)


# add admin & users
user=User(id=1,username='admin',fullname='admin',position='admin',teamId=1, roomId=0)
user.set_password('admin')
db.session.add(user)


user=User(id=2,username='scardinof',fullname='Francesco',position='Physics PhD 33',teamId=2, roomId=1)
user.set_password('scardinof')
db.session.add(user)


user=User(id=3,username='dicarloma',fullname='Matteo',position='Physics PhD 33',teamId=2, roomId=1)
user.set_password('dicarloma')
db.session.add(user)


user=User(id=4,username='udinamatt',fullname='Mattia',position='Physics PhD 34',teamId=2, roomId=1)
user.set_password('udinamatt')
db.session.add(user)


user=User(id=5,username='castrogon',fullname='Goncalo',position='Physics PhD 34',teamId=2, roomId=1)
user.set_password('castrogon')
db.session.add(user)


user=User(id=6,username='raposogui',fullname='Guilherme',position='Physics PhD 33',teamId=2, roomId=1)
user.set_password('raposogui')
db.session.add(user)


user=User(id=7,username='piomboric',fullname='Riccardo',position='Physics PhD 34',teamId=2, roomId=1)
user.set_password('piomboric')
db.session.add(user)


user=User(id=8,username='cherubini',fullname='Marco',position='Physics PhD 34',teamId=2, roomId=1)
user.set_password('cherubini')
db.session.add(user)


user=User(id=9,username='baldanpao',fullname='Paolo',position='Physics PhD 33',teamId=2, roomId=1)
user.set_password('baldanpao')
db.session.add(user)


user=User(id=10,username='vendittig',fullname='Giulia',position='Physics PhD 34',teamId=2, roomId=1)
user.set_password('vendittig')
db.session.add(user)


user=User(id=11,username='cascioliv',fullname='Valerio',position='Physics PhD 33',teamId=2, roomId=2)
user.set_password('cascioliv')
db.session.add(user)


user=User(id=12,username='neuwirthj',fullname='Julia',position='Physics PhD 35',teamId=2, roomId=2)
user.set_password('neuwirthj')
db.session.add(user)


user=User(id=13,username='maggioeli',fullname='Elisa',position='Physics PhD 34',teamId=2, roomId=2)
user.set_password('maggioeli')
db.session.add(user)


user=User(id=14,username='morettifa',fullname='Fabio',position='Physics PhD 33',teamId=2, roomId=2)
user.set_password('morettifa')
db.session.add(user)


user=User(id=15,username='delpretem',fullname='Matteo',position='Physics PhD 34',teamId=2, roomId=2)
user.set_password('delpretem')
db.session.add(user)


user=User(id=16,username='piovanoga',fullname='Gabriel',position='Physics PhD 34',teamId=2, roomId=2)
user.set_password('piovanoga')
db.session.add(user)


user=User(id=17,username='rotamiche',fullname='Michele',position='Physics PhD 33',teamId=2, roomId=2)
user.set_password('rotamiche')
db.session.add(user)


user=User(id=18,username='perrupato',fullname='Gianmarco',position='Physics PhD 34',teamId=2, roomId=3)
user.set_password('perrupato')
db.session.add(user)


user=User(id=19,username='campionst',fullname='Stefano',position='IRAP PhD 32',teamId=2, roomId=3)
user.set_password('campionst')
db.session.add(user)


user=User(id=20,username='dicarlolu',fullname='Luca',position='Physics PhD 34',teamId=2, roomId=3)
user.set_password('dicarlolu')
db.session.add(user)


user=User(id=21,username='gabrielef',fullname='Francesco',position='Physics PhD 35',teamId=2, roomId=3)
user.set_password('gabrielef')
db.session.add(user)


user=User(id=22,username='pisegnagi',fullname='Giulia',position='Physics PhD 34',teamId=2, roomId=3)
user.set_password('pisegnagi')
db.session.add(user)


user=User(id=23,username='carmonavi',fullname='Viridiana',position='Physics PhD 33',teamId=2, roomId=3)
user.set_password('carmonavi')
db.session.add(user)


user=User(id=24,username='cullaanto',fullname='Antonio',position='Physics PhD 34',teamId=2, roomId=3)
user.set_password('cullaanto')
db.session.add(user)


user=User(id=25,username='francosil',fullname='Silvia',position='SBAI PhD 34',teamId=2, roomId=3)
user.set_password('francosil')
db.session.add(user)


user=User(id=26,username='camerinfa',fullname='Fabrizio',position='SBAI PhD 33',teamId=2, roomId=3)
user.set_password('camerinfa')
db.session.add(user)


user=User(id=27,username='delmonteg',fullname='Giovanni',position='Physics PhD 34',teamId=2, roomId=3)
user.set_password('delmonteg')
db.session.add(user)


user=User(id=28,username='pierinilo',fullname='Lorenzo',position='Physics PhD 35',teamId=2, roomId=3)
user.set_password('pierinilo')
db.session.add(user)


user=User(id=29,username='giovannet',fullname='Eleonora',position='Physics PhD 35',teamId=2, roomId=3)
user.set_password('giovannet')
db.session.add(user)


user=User(id=30,username='nicoletti',fullname='Flavio',position='Physics PhD 35',teamId=2, roomId=3)
user.set_password('nicoletti')
db.session.add(user)


user=User(id=31,username='cavaliere',fullname='Angelo',position='Physics PhD 33',teamId=2, roomId=3)
user.set_password('cavaliere')
db.session.add(user)


user=User(id=32,username='diazrafae',fullname='Rafael',position='Physics PhD 33',teamId=2, roomId=3)
user.set_password('diazrafae')
db.session.add(user)


user=User(id=33,username='sabatucci',fullname='Andrea',position='Physics PhD 35',teamId=2, roomId=3)
user.set_password('sabatucci')
db.session.add(user)


user=User(id=34,username='piacentin',fullname='Stefano',position='Physics PhD 34',teamId=2, roomId=3)
user.set_password('piacentin')
db.session.add(user)


user=User(id=35,username='pagailari',fullname='Ilaria',position='Physics PhD 33',teamId=2, roomId=4)
user.set_password('pagailari')
db.session.add(user)


user=User(id=36,username='qulaghasi',fullname='Shadi',position='Physics PhD 35',teamId=2, roomId=4)
user.set_password('qulaghasi')
db.session.add(user)


user=User(id=37,username='platiandr',fullname='Andrea',position='Physics PhD 34',teamId=2, roomId=4)
user.set_password('platiandr')
db.session.add(user)


user=User(id=38,username='longeliza',fullname='Elizabeth',position='Physics PhD 35',teamId=2, roomId=4)
user.set_password('longeliza')
db.session.add(user)


user=User(id=39,username='ferrettif',fullname='Federica',position='Physics PhD 34',teamId=2, roomId=4)
user.set_password('ferrettif')
db.session.add(user)


user=User(id=40,username='nieddajac',fullname='Jacopo',position='Physics PhD 35',teamId=2, roomId=4)
user.set_password('nieddajac')
db.session.add(user)


user=User(id=41,username='lingettig',fullname='Giuseppe',position='Physics PhD 35',teamId=2, roomId=4)
user.set_password('lingettig')
db.session.add(user)


user=User(id=42,username='falsiludo',fullname='Ludovica',position='SBAI PhD 34',teamId=2, roomId=4)
user.set_password('falsiludo')
db.session.add(user)


user=User(id=43,username='borrafran',fullname='Francesco',position='Physics PhD 34',teamId=2, roomId=4)
user.set_password('borrafran')
db.session.add(user)


user=User(id=44,username='savaresim',fullname='Matteo',position='SBAI PhD 35',teamId=2, roomId=4)
user.set_password('savaresim')
db.session.add(user)


user=User(id=45,username='vagliomas',fullname='Massimo',position='Physics PhD 35',teamId=2, roomId=4)
user.set_password('vagliomas')
db.session.add(user)


user=User(id=46,username='silvettif',fullname='Federico',position='Physics PhD 35',teamId=2, roomId=4)
user.set_password('silvettif')
db.session.add(user)


user=User(id=47,username='benedetti',fullname='Marco',position='Physics PhD 35',teamId=2, roomId=4)
user.set_password('benedetti')
db.session.add(user)


user=User(id=48,username='marcheseg',fullname='Guglielmo',position='Physics PhD 35',teamId=2, roomId=4)
user.set_password('marcheseg')
db.session.add(user)


user=User(id=49,username='hochfranc',fullname='Francesco',position='Physics PhD 35',teamId=2, roomId=5)
user.set_password('hochfranc')
db.session.add(user)


user=User(id=50,username='valerimau',fullname='Mauro',position='Physics PhD 33',teamId=2, roomId=5)
user.set_password('valerimau')
db.session.add(user)


user=User(id=51,username='recchiaan',fullname='Andrea',position='Physics PhD 35',teamId=2, roomId=5)
user.set_password('recchiaan')
db.session.add(user)


user=User(id=52,username='poderinid',fullname='Davide',position='Physics PhD 33',teamId=2, roomId=5)
user.set_password('poderinid')
db.session.add(user)


user=User(id=53,username='laneveale',fullname='Alessandro',position='Physics PhD 35',teamId=2, roomId=5)
user.set_password('laneveale')
db.session.add(user)


user=User(id=54,username='dibiagioa',fullname='Andrea',position='Physics PhD 34',teamId=2, roomId=5)
user.set_password('dibiagioa')
db.session.add(user)


user=User(id=55,username='espositoc',fullname='Chiara',position='Physics PhD 34',teamId=2, roomId=5)
user.set_password('espositoc')
db.session.add(user)


user=User(id=56,username='geraldian',fullname='Andrea',position='Physics PhD 33',teamId=2, roomId=5)
user.set_password('geraldian')
db.session.add(user)


user=User(id=57,username='supranoal',fullname='Alessia',position='Physics PhD 34',teamId=2, roomId=5)
user.set_password('supranoal')
db.session.add(user)


user=User(id=58,username='meucciman',fullname='Manuel',position='Physics PhD 34',teamId=2, roomId=5)
user.set_password('meucciman')
db.session.add(user)


user=User(id=59,username='carincima',fullname='Massimo',position='IRAP PhD 33',teamId=2, roomId=5)
user.set_password('carincima')
db.session.add(user)

user=User(id=60,username='mancanisa',fullname='Salvatore',position='Physics PhD 34',teamId=2, roomId=5)
user.set_password('mancanisa')
db.session.add(user)

user=User(id=61,username='polacchib',fullname='Beatrice',position='Physics PhD 36',teamId=2, roomId=5)
user.set_password('polacchib')
db.session.add(user)


db.session.commit()
